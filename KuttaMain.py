from tabulate import tabulate


# __________ Runge-Kutta 4th order __________
def function_definition(x, y):
    # return pow(x, 2) - 2 * y - 4 * x
    return -2 * x - y


def compute_k1_kf(x, y):
    return function_definition(x, y)


def compute_k2_kf(x, y, h, k1):
    x_param = x + (1 / 2) * h
    y_param = y + (1 / 2) * k1 * h
    return function_definition(x_param, y_param)


def compute_k3_kf(x, y, h, k2):
    x_param = x + (1 / 2) * h
    y_param = y + (1 / 2) * k2 * h
    return function_definition(x_param, y_param)


def compute_k4_kf(x, y, h, k3):
    x_param = x + h
    y_param = y + k3 * h
    return function_definition(x_param, y_param)


def compute_next_y_kf(y, k1, k2, k3, k4, h):
    return y + (h / 6) * (k1 + 2 * k2 + 2 * k3 + k4)


def compute_rows_f(i):
    global x, h, table_kutta_fourth
    row_fourth = []
    y_new_f = table_kutta_fourth[i - 1][7]
    k1_f = compute_k1_kf(x, y_new_f)
    k2_f = compute_k2_kf(x, y_new_f, h, k1_f)
    k3_f = compute_k3_kf(x, y_new_f, h, k2_f)
    k4_f = compute_k4_kf(x, y_new_f, h, k3_f)
    next_y_f = compute_next_y_kf(y_new_f, k1_f, k2_f, k3_f, k4_f, h)
    row_fourth.extend([i, x, y_new_f, k1_f, k2_f, k3_f, k4_f, next_y_f])
    table_kutta_fourth.append(row_fourth)


# __________ KUTTA MERSON __________

def compute_k1_m(x, y, h):
    return h * function_definition(x, y)


def compute_k2_m(x, y, h, k1):
    x_param = x + (h / 3)
    y_param = y + (k1 / 3)
    return h * function_definition(x_param, y_param)


def compute_k3_m(x, y, h, k1, k2):
    x_param = x + (h / 3)
    y_param = y + (k1 / 6) + (k2 / 6)
    return h * function_definition(x_param, y_param)


def compute_k4_m(x, y, h, k1, k3):
    x_param = x + (h / 2)
    y_param = y + (k1 / 8) + (3 / 8) * k3
    return h * function_definition(x_param, y_param)


def compute_k5_m(x, y, h, k1, k3, k4):
    x_param = x + h
    y_param = y + (k1 / 2) - (3 / 2) * k3 + 2 * k4
    return h * function_definition(x_param, y_param)


def compute_next_y_m(y, k1, k4, k5):
    return y + ((k1 + 4 * k4 + k5) / 6)


def compute_rows_m(i):
    global x, h, table_kutta_merson
    row_merson = []
    y_new_m = table_kutta_merson[i - 1][8]
    k1_m = compute_k1_m(x, y_new_m, h)
    k2_m = compute_k2_m(x, y_new_m, h, k1_m)
    k3_m = compute_k3_m(x, y_new_m, h, k1_m, k2_m)
    k4_m = compute_k4_m(x, y_new_m, h, k1_m, k3_m)
    k5_m = compute_k5_m(x, y_new_m, h, k1_m, k3_m, k4_m)
    next_y_m = compute_next_y_m(y_new_m, k1_m, k4_m, k5_m)

    row_merson.extend([i, x, y_new_m, k1_m, k2_m, k3_m, k4_m, k5_m, next_y_m])
    table_kutta_merson.append(row_merson)


def print_whole_table():
    print("Runge Kutta de cuarto orden")
    print(tabulate(table_kutta_fourth, headers=["i", "xi", "yi", "k1", "k2", "k3", "k4", "yi+1"]))
    print("Runge Kutta Merson")
    print(tabulate(table_kutta_merson, headers=["i", "xi", "yi", "k1", "k2", "k3", "k4", "k5", "yi+1"]))


def print_only_results_f():
    table = []
    for row in table_kutta_fourth:
        values = [row[1], row[2]]
        table.append(values)

    print(tabulate(table, headers=["x", "y"]))


def print_only_results_m():
    table = []
    for row in table_kutta_merson:
        values = [row[1], row[2]]
        table.append(values)

    print(tabulate(table, headers=["x", "y"]))


def print_only_results():
    print("Runge Kutta de cuarto orden")
    print_only_results_f()
    print("Runge Kutta Merson")
    print_only_results_m()


def compute_kutta_fourth_order(i):
    global x, h
    if i == 0:
        row_fourth = []
        row_fourth.extend([i, x, y])

        k1_f = compute_k1_kf(x, y)
        k2_f = compute_k2_kf(x, y, h, k1_f)
        k3_f = compute_k3_kf(x, y, h, k2_f)
        k4_f = compute_k4_kf(x, y, h, k3_f)
        next_y_f = compute_next_y_kf(y, k1_f, k2_f, k3_f, k4_f, h)
        row_fourth.extend([k1_f, k2_f, k3_f, k4_f, next_y_f])
        table_kutta_fourth.append(row_fourth)
    else:
        compute_rows_f(i)


def compute_kutta_merson(i):
    global x, h
    if i == 0:
        row_merson = []
        row_merson.extend([i, x, y])
        k1_m = compute_k1_m(x, y, h)
        k2_m = compute_k2_m(x, y, h, k1_m)
        k3_m = compute_k3_m(x, y, h, k1_m, k2_m)
        k4_m = compute_k4_m(x, y, h, k1_m, k3_m)
        k5_m = compute_k5_m(x, y, h, k1_m, k3_m, k4_m)
        next_y_m = compute_next_y_m(y, k1_m, k4_m, k5_m)

        row_merson.extend([k1_m, k2_m, k3_m, k4_m, k5_m, next_y_m])
        table_kutta_merson.append(row_merson)
    else:
        compute_rows_m(i)


def compute_error():
    global table_kutta_fourth, table_kutta_merson, num_itr
    table_error = []
    for i in range(num_itr + 1):
        if i == 0:
            continue
        table_error.append(abs(table_kutta_fourth[i][2] - table_kutta_merson[i][2]))

    print("Error entre los metodos")
    print(table_error)


answer = "s"
while answer == "s":
    # Main program
    print("Runge-Kutta 4 orden y Merson")
    x = float(input("Ingresa el valor inicial de x: "))
    y = float(input("Ingresa el valor inicial de y: "))
    h = float(input("Ingresa el valor de h: "))
    tolerance: int = int(input("Ingresa la tolerancia: "))
    display_whole_table = input("Quieres ver la tabla con los valores de los ks? (S/N): ")
    table_kutta_fourth = []
    table_kutta_merson = []
    num_itr = int((tolerance - x) / h)

    for i in range(num_itr + 1):
        if i >= 1:
            x += h

        compute_kutta_fourth_order(i)
        compute_kutta_merson(i)

    if display_whole_table == "s":
        print_whole_table()
        compute_error()
    else:
        print_only_results()
        compute_error()

    answer = input("Quieres Ingresar nuevos valores iniciales? (S/N):")
